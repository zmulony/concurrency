package pl.edu.pwr.deadlock;

public class Deadlock {

	static class DeadlockingThread1 extends Thread {
		public void method1() {
			synchronized (String.class) {
				System.out.println("Aquired lock on String.class object");

				synchronized (Integer.class) {
					System.out.println("Aquired lock on Integer.class object");
				}
			}
		}

		@Override
		public void run() {
			for (int i = 0; i < 5; i++) {
				method1();
			}
		}
	}

	static class DeadlockingThread2 extends Thread {

		public void method2() {
			synchronized (Integer.class) {
				System.out.println("Aquired lock on Integer.class object");

				synchronized (String.class) {
					System.out.println("Aquired lock on String.class object");
				}
			}
		}

		@Override
		public void run() {
			for (int i = 0; i < 5; i++) {
				method2();
			}
		}
	}

	public static void main(String[] args) {
		Thread thread1 = new DeadlockingThread1();
		Thread thread2 = new DeadlockingThread2();
		thread1.start();
		thread2.start();

		try {
			thread1.join();
			thread2.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
