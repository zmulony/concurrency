package pl.edu.pwr.synchronization;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.Test;

public class SingletonTest {

	@Test
	public void createSingleton() {
		Callable<Singleton> task = new Callable<Singleton>() {

			@Override
			public Singleton call() throws Exception {
				Thread.sleep((long)Math.random()*1000);
				return Singleton.getInstance();
			}
			
		};
		
		try {
			List<Future<Singleton>> futures = runTest(task);
			int value1 = futures.get(0).get().getValue();
			int value2 = futures.get(1).get().getValue();
			int value3 = futures.get(2).get().getValue();
			Assert.assertEquals(value1, value2);
			Assert.assertEquals(value2, value3);
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
		
	}
	

	private List<Future<Singleton>> runTest(Callable<Singleton> task) throws InterruptedException, ExecutionException {
		ExecutorService pool = Executors.newFixedThreadPool(3);
		Collection<Callable<Singleton>> tasks = Collections.nCopies(3, task);
		List<Future<Singleton>> all = pool.invokeAll(tasks);
		pool.awaitTermination(1, TimeUnit.SECONDS);
		return all;
	}
	
}
